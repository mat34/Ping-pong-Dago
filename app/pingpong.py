from flask import Flask
from flask import jsonify
from flask import request
app = Flask(__name__)

count = 0

@app.route('/')
def docker():
  return 'Docker Web Service'
  
  
@app.route('/count')
def counter():
  global count
  response = jsonify(pingCount = str(count))
  return response
  

@app.route('/ping')
def pingPong():
  global count
  count += 1
  response = jsonify(message = "pong")
  return response


@app.route('/reset')
def reset():
  global count
  count = 0
  return "la valeur du compteur a ete reinitialisee"

if __name__ == '__main__':
  app.run(debug=True, host='0.0.0.0')

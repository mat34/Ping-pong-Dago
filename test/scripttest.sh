#!/bin/bash
URL=http://127.0.0.1:5000


if [ ! $(curl -s $URL/count | jq  '.count') == "1" ]
then
  echo "L'incrémentation du compteur ne fonctionne pas"
  exit 1

fi


if [ ! $(curl -s $URL/count | jq '.count') == "0" ]
then
  echo "L'initialisation du compteur ne fonctionne pas"
  exit 1

fi


if [ ! $(curl -s $URL/ping | jq '.message')=="pong" ]
then
  echo "La page /ping ne fonctionne pas"
  exit 1
fi




echo "L application fonctionne correctement"
exit 2

